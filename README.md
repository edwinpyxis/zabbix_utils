# Zabbix utils

Herramienta de utilidad que permite a los procesos batch enviar estados para ser monitoreados por Zabbix Server

## Prerequisito

* Tener instalado y configurado **zabbix-agent** y **zabbix-sender** en el equipo donde se ejecutaran los procesos batch

Para mas información ver la pagina:

https://pyxisjira.atlassian.net/wiki/spaces/INFRA/pages/790691865/MONITOREAR+PROCESOS+BATCH


## Ejemplo


Al clonar el repositorio encontraran un script de ejemplo **run_example.sh** que muestra el uso básico de la herramienta, se detalla a continuación:


* Se define el directorio de trabajo y donde va a quedar el archivo de log, luego se importan las librerias utils para su uso


```bash
#!bin/bash
WORKDIR=$(dirname "${BASH_SOURCE[0]}")
LOGFILE=$WORKDIR/logs/run-example.log

. $WORKDIR/script_utils.sh $LOGFILE
. $WORKDIR/zabbix_utils.sh $LOGFILE
```
* Se crea la función **exit_** que reemplaza al del script **script_utils.sh**, la diferencia es que este envía el estado a Zabbix Server

```bash
function exit_ (){
    zabbix_send 1 ## Envia estado "1" a Zabbix Server
    exit 1
}
```

* Se ejecutan 2 comandos, uno que se ejecuta correctamente y el otro que da un error el cual envia el estado 1 al Zabbix

```bash
echo "ejecuto o hago algo"
check $? ## El chequeo devuelve resultado 0 por lo que esta OK

hagomal ## Este comando  no existe
check $?  ## Al hacer el chequeo devuelve un resultado distinto a "0" por el cual el programa termina, envía el estado "1" a Zabbix Server y saltará un alerta
```
* Finaliza el script en caso de no haber errores, y envía el estado "0" a Zabbix Server

```bash
#fin script
zabbix_send 0 ## Envia estado "0" a Zabbix Server
```

#!/usr/bin/env bash

# PARA EL USO DE ESTE SCRIPT, SE REQUIERE EL UTILS.SH
VERSION=1.0
ZBX_WORKDIR=$(dirname "${BASH_SOURCE[0]}")
LOGFILE=$1
CONFIG_FILE="$ZBX_WORKDIR/zabbix_sender.conf"

function zabbix_send () {
    if [ $1 -eq 0 ] ; then
        $ZABBIX_SENDER -c $ZABBIX_SENDER_CONF -k $ZABBIX_KEY -o $ZABBIX_OK &>> $LOGFILE
        msg=OK
    else
        $ZABBIX_SENDER -c $ZABBIX_SENDER_CONF -k $ZABBIX_KEY -o $ZABBIX_NOK &>> $LOGFILE
        msg=ERROR
    fi
    log "ENVIO $msg A ZABBIX"
}

# CARGAR ARCHIVO DE CONFIGURACIÓN
if [ -e "${CONFIG_FILE}" ];then
        . "${CONFIG_FILE}"
else
        log "ERROR" "ARCHIVO DE CONFIGURACION ${CONFIG_FILE} NO ENCONTRADO"
        exit 1
fi

# VALIDAR CONFIGURACIÓN DE ZABBIX SENDER NO ESTÉ VACIA
if [ -z "$ZABBIX_SENDER" ] || [ -z "$ZABBIX_SENDER_CONF" ] || [ -z "$ZABBIX_KEY" ] || [ -z "$ZABBIX_OK" ] || [ -z "$ZABBIX_NOK" ]; then
    log "ERROR" "EXISTEN PARAMETROS VACIOS EN ${CONFIG_FILE}"
    exit 1
fi

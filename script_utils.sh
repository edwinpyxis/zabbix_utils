#!/usr/bin/env bash
VERSION=1.0
LOGFILE=$1

# FUNCIÓN PARA LOGUEAR EN ARCHIVO
function log(){
	date_=$(date +%Y-%m-%d" - "%H:%M:%S)
	if [ "$1" = "ERROR" ] || [ "$1" = "OK" ]; then
		level=$1 
		msg=$2
	else
		level="INFO"
		msg=$1
	fi
	echo -e "${date_} - ${level} - ${msg}"
	echo -e "${date_} - ${level} - ${msg}" >> ${LOGFILE}
}

#sobreescribir en script padres para controlar el fin del script
function exit_ (){
	if [ "$1" = "" ] ; then
		exit 1
	else
		exit $1
	fi
}
trap exit_ ERR #EXIT #capturo las salidas de error de los scripts para salir controlado por esta funcion

# FUNCIÓN QUE VALIDA EL CODIGO DE SALIDA DE UN COMANDO, SI ES DISTINTO A 0 ( OK ), TERMINA EL PROCESO
function check() {
	local code=$1
	local msg=$2
   	if [ ! $code -eq 0 ] ; then
        	log "ERROR" "$msg"
        	exit_ 1
   	fi
}

# FUNCIÓN PARA VALIDAR SI UNA RUTA EXISTE, EN CASO QUE NO, TERMINA EL PROCESO
# USO: exist_path "/tmp/a.txt"
function exist_path() {
    if [ ! -e "$1" ]
    then
        log "NO EXISTE $1"
        exit_ 1
    fi
}

### VALIDACIONES

#VALIDO LOG
#este caso no puedo llamar a log :)

if [ "$LOGFILE" = "" ] ; then
	LOGFILE=output.log
fi

log_folder=$(dirname $LOGFILE)
if [ ! -d "$log_folder" ] ; then
	mkdir "$log_folder"
	if [ ! $? -eq 0 ] ; then
		echo ""
		echo "ERROR - No existe carpeta de log $log_folder"
		echo ""
		exit 1
	fi
fi
log "script-utils: 1.0"
